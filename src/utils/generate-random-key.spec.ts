import test from 'ava';

import { generateRandomKey } from './generate-random-key.js';

test('generate two different keys', (t) => {
    t.plan(1);

    const key1 = generateRandomKey();
    const key2 = generateRandomKey();

    t.not(key1, key2);
});

test('generate random key of requested length', (t) => {
    t.plan(10);
    const lengths = [1, 50, 100, 150, 200, 250, 300, 350, 400, 450];

    for (const length of lengths) {
        const key = generateRandomKey(length);

        t.is(key.length, length);
    }
});

test('generate key of 254 if no length is provided', (t) => {
    t.plan(1);

    const key = generateRandomKey();

    t.is(key.length, 254);
});
