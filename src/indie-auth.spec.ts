import test from 'ava';

import { IndieAuth } from './indie-auth.js';

const initialFetch = IndieAuth.fetch;

test.after.always(() => {
    IndieAuth.fetch = initialFetch;
});

test('create with state', (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        [],
        {
            existingStates: ['123'],
        }
    );

    const validationResult = indieAuth.validateState('123');

    t.is(validationResult, true);
});

test('state should expire after default expiry period', async (t) => {
    t.timeout(40_000);
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        [],
        {
            existingStates: ['123'],
        }
    );

    const validationResult = indieAuth.validateState('123');

    await new Promise((resolve) => setTimeout(resolve, 35_000));

    t.is(validationResult, true);
});

test('state should expire after set expiry period', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        [],
        {
            existingStates: ['123'],
            stateExpiryTime: 5000,
        }
    );

    const validationResult = indieAuth.validateState('123');

    await new Promise((resolve) => setTimeout(resolve, 10_000));

    t.is(validationResult, true);
});

test('construct the auth endpoint url', (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com/re-direct',
        [],
        {
            existingStates: ['123'],
            stateExpiryTime: 5000,
        }
    );

    const result = indieAuth.constructAuthUrl(
        'https://authendpoint.example.com',
        'https://me.example.com',
        ['create']
    );

    const resultRegEx = new RegExp(
        'https:\\/\\/authendpoint\\.example\\.com\\/\\?me=https%3A%2F%2Fme\\.example\\.com&client_id=https%3A%2F%2Fwww\\.example\\.com%2F&redirect_uri=https%3A%2F%2Fwww\\.example\\.com%2Fre-direct&state=[a-zA-Z0-9]{50}&scope=create&response_type=id'
    );

    t.is(resultRegEx.test(result), true);
});

test('should create new state when creating auth endpoint url', (t) => {
    t.plan(4);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com/re-direct',
        [],
        {
            existingStates: ['123'],
            stateExpiryTime: 5000,
        }
    );

    const result1 = indieAuth.constructAuthUrl(
        'https://authendpoint.example.com',
        'https://me.example.com',
        ['create']
    );
    const result2 = indieAuth.constructAuthUrl(
        'https://authendpoint.example.com',
        'https://me.example.com',
        ['create']
    );

    const url1 = new URL(result1);
    const state1 = url1.searchParams.get('state');

    const url2 = new URL(result2);
    const state2 = url2.searchParams.get('state');

    if (state1 === null) {
        t.fail('State 1 was null.');
        return;
    }

    if (state2 === null) {
        t.fail('State 2 was null.');
        return;
    }

    t.not(result1, result2);
    t.not(state1, state2);
    t.is(indieAuth.validateState(state1), true);
    t.is(indieAuth.validateState(state2), true);
});

test('not remove state after validation', (t) => {
    t.plan(2);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        [],
        {
            existingStates: ['123'],
        }
    );

    const validationResult = indieAuth.validateState('123');
    const validationResult2 = indieAuth.validateState('123');

    t.is(validationResult, true);
    t.is(validationResult2, false);
});

test('throw error if auth endpoint returns none 200', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        [],
        {
            existingStates: ['123'],
        }
    );

    IndieAuth.fetch = () => Promise.resolve(new Response('', { status: 400 }));

    await t.throwsAsync(
        () => indieAuth.validateCode('123', 'https://www.example.com'),
        { message: 'Auth endpoint returned a 400.' }
    );
});

test('throw error if auth endpoint returns a none object body', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        [],
        {
            existingStates: ['123'],
        }
    );

    IndieAuth.fetch = () =>
        Promise.resolve(new Response('true', { status: 200 }));

    await t.throwsAsync(
        () => indieAuth.validateCode('123', 'https://www.example.com'),
        { message: 'Auth endpoint returned an invalid body.' }
    );
});

test('throw error if auth endpoint returns without a me', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        [],
        {
            existingStates: ['123'],
        }
    );

    IndieAuth.fetch = () =>
        Promise.resolve(new Response('{ "you": "fail" }', { status: 200 }));

    await t.throwsAsync(
        () => indieAuth.validateCode('123', 'https://www.example.com'),
        { message: 'Auth endpoint did not return a "me" property.' }
    );
});

test('return false if me is not a permitted user', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        ['https://www.example.com'],
        {
            existingStates: ['123'],
        }
    );

    IndieAuth.fetch = () =>
        Promise.resolve(
            new Response('{ "me": "https://www.fail.com" }', { status: 200 })
        );

    const result = await indieAuth.validateCode(
        '123',
        'https://www.example.com'
    );

    t.is(result, false);
});

test('return true if me is a permitted user', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        ['https://www.example.com'],
        {
            existingStates: ['123'],
        }
    );

    IndieAuth.fetch = () =>
        Promise.resolve(
            new Response('{ "me": "https://www.example.com" }', { status: 200 })
        );

    const result = await indieAuth.validateCode(
        '123',
        'https://www.example.com'
    );

    t.is(result, true);
});

test('return true if me is a permitted user with minor differnces', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth(
        'https://www.example.com',
        'https://www.example.com',
        ['https://www.example.com'],
        {
            existingStates: ['123'],
        }
    );

    IndieAuth.fetch = () =>
        Promise.resolve(
            new Response('{ "me": "https://www.example.com/" }', {
                status: 200,
            })
        );

    const result = await indieAuth.validateCode(
        '123',
        'https://www.example.com'
    );

    t.is(result, true);
});
