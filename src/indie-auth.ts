import { generateRandomKey } from './utils/generate-random-key.js';
import { IndieAuthError } from './errors.js';

interface IndieAuthOptions {
    stateExpiryTime: number;
    existingStates: string[];
}

const DEFAULT_OPTIONS: IndieAuthOptions = {
    stateExpiryTime: 30_000,
    existingStates: [],
};

export class IndieAuth {
    private clientId: string;
    private redirectUri: string;
    private states: Set<string> = new Set();
    private stateExpiryTime: number | undefined;
    private permittedUsers: Set<string> = new Set();

    static fetch = fetch;

    constructor(
        clientId: string,
        redirectUri: string,
        permittedUsers: string[],
        options: Partial<IndieAuthOptions> | undefined
    ) {
        this.clientId = this.normaliseUrl(clientId);
        this.redirectUri = this.normaliseUrl(redirectUri);

        this.addPermittedUsers(permittedUsers);
        this.setOptions(options);
    }

    private setOptions(options: Partial<IndieAuthOptions> | undefined) {
        const combinedOptions: IndieAuthOptions = {
            ...DEFAULT_OPTIONS,
            ...options,
        };

        this.stateExpiryTime = combinedOptions.stateExpiryTime;
        for (const state of combinedOptions.existingStates) {
            this.addState(state);
        }
    }

    private addPermittedUsers(users: string[]) {
        for (const user of users) {
            const userUrl = this.normaliseUrl(user);
            this.permittedUsers.add(userUrl);
        }
    }

    private normaliseUrl(url: string): string {
        return new URL(url).toString();
    }

    /**
     * creates a random state string which self expires
     */
    private createState() {
        const state = generateRandomKey(50);

        this.addState(state);

        return state;
    }

    private addState(state: string) {
        this.states.add(state);

        setTimeout(() => this.states.delete(state), this.stateExpiryTime);
    }

    validateState(state: string): boolean {
        const isStateValid = this.states.has(state);

        this.states.delete(state);

        return isStateValid;
    }

    async validateCode(code: string, authEndpoint: string): Promise<boolean> {
        const response = await IndieAuth.fetch(authEndpoint, {
            method: 'POST',
            body: new URLSearchParams({
                code,
                client_id: this.clientId,
                redirect_uri: this.redirectUri,
            }),
        });

        if (response.status !== 200) {
            throw new IndieAuthError(
                `Auth endpoint returned a ${response.status}.`
            );
        }

        const body = await response.json();

        if (typeof body !== 'object' || body === null) {
            throw new IndieAuthError('Auth endpoint returned an invalid body.');
        }

        if (!('me' in body)) {
            throw new IndieAuthError(
                'Auth endpoint did not return a "me" property.'
            );
        }

        const meUrl = this.normaliseUrl(body.me);

        return this.permittedUsers.has(meUrl);
    }

    constructAuthUrl(
        authEndpoint: string,
        me: string,
        scopes: string[]
    ): string {
        const authURL = new URL(authEndpoint);
        const state = this.createState();

        authURL.searchParams.set('me', me);
        authURL.searchParams.set('client_id', this.clientId);
        authURL.searchParams.set('redirect_uri', this.redirectUri);
        authURL.searchParams.set('state', state);
        authURL.searchParams.set('scope', scopes.join(','));
        authURL.searchParams.set('response_type', 'id');

        return authURL.toString();
    }
}
